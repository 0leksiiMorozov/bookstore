﻿using Store.BusinessLogicLayer.Models.Base;
using Store.BusinessLogicLayer.Models.PrintingEdition;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Store.BusinessLogicLayer.Services.Interfaces
{
    public interface IPrintingEditionService
    {
        Task <PrintingEditionsModel> GetAllPrintingEditionsAsync(PrintingEditionsBllFilterModel model);
        Task<BaseModel> CreatePrintingEditionAsync(PrintingEditionsModelItem model);
        Task<BaseModel> UpdatePrintingEditionAsync(PrintingEditionsModelItem model);
        Task<BaseModel> DeletePrintingEditionAsync(long id);
        Task<PrintingEditionsModelItem> GetPrintingEditionById(long id);
    }
}
