﻿using Store.BusinessLogicLayer.Models.Base;
using Store.BusinessLogicLayer.Models.Cart;
using Store.BusinessLogicLayer.Models.Orders;
using System.Threading.Tasks;

namespace Store.BusinessLogicLayer.Services.Interfaces
{
    public interface IOrderService
    {
        Task<BaseModel> DeleteOrderAsync(long id);
        Task<OrdersModel> GetAllOrdersAsync(OrdersBllFilterModel model);
        Task<BaseModel> MakeOrderAsync(CartModelItem model, long userId);
        Task<OrdersModel> GetUserOrdersAsync(long id);
    }
}
