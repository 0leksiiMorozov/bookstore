﻿using Store.BusinessLogicLayer.Models.Authors;
using Store.BusinessLogicLayer.Models.Base;
using System.Threading.Tasks;

namespace Store.BusinessLogicLayer.Services.Interfaces
{
    public interface IAuthorService
    {
        Task<BaseModel> CreateAuthorAsync(AuthorsModelItem model);
        Task<BaseModel> UpdateAuthorAsync(AuthorsModelItem model);
        Task<BaseModel> DeleteAuthorAsync(long id);
        Task<AuthorsModelItem> GetAuthorByIdAsync(long id);
        Task<AuthorsModel> GetAllAuthorsAsync(AuthorsBllFilteringModel model);
        Task<AuthorsModel> GetAuthorsForProductCreationAsync();
    }
}
