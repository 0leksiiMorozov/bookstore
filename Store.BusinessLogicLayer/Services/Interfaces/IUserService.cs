﻿using Microsoft.AspNetCore.Http;
using Store.BusinessLogicLayer.Models.Base;
using Store.BusinessLogicLayer.Models.Users;
using System;
using System.Threading.Tasks;

namespace Store.BusinessLogicLayer.Services.Interfaces
{
    public interface IUserService
    {     
        Task<UserModelItem> GetUserByIdAsync(long id);
        Task<UserModel> GetAllUsersAsync(UsersBllFilterModel model, long adminId);
        Task<BaseModel> EditUserAsync(UserModelItem model, long id);
        Task<BaseModel> DeleteUserAsync(long id);
        Task<BaseModel> BlockUserAsync(long id, bool block);
        Task<BaseModel> SetPhotoAsync(UserModelItem model);
    }
}
