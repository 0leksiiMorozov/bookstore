﻿using Store.BusinessLogicLayer.Mappers;
using Store.BusinessLogicLayer.Models.Base;
using Store.BusinessLogicLayer.Models.Cart;
using Store.BusinessLogicLayer.Models.OrderItem;
using Store.BusinessLogicLayer.Models.Orders;
using Store.BusinessLogicLayer.Services.Interfaces;
using Store.DataAccessLayer.Repositories.EFRepositories;
using Store.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Store.BusinessLogicLayer.Common.Constants.Error.Constants;

namespace Store.BusinessLogicLayer.Services
{
    public class OrderService : IOrderService
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IOrderRepository _orderRepository;
        public OrderService(IOrderRepository orderRepository, IPaymentRepository paymentRepository, IOrderItemRepository orderItemRepository)
        {
            _orderRepository = orderRepository;
            _paymentRepository = paymentRepository;
            _orderItemRepository = orderItemRepository;
        }

        private async Task<BaseModel> CreateOrderAsync(OrdersModelItem model)
        {
            var response = new BaseModel();
            if (model == null)
            {
                response.Errors.Add(ErrorConstants.ModelIsNull);
                return response;
            }
            var order = OrderMapper.MapModelToEntity(model);
            var result = await _orderRepository.CreateAsync(order);
            if (!result)
            {
                response.Errors.Add(ErrorConstants.ImpossibleToCreateOrder);
            }
            return response;
        }             

        private async Task<BaseModel> CreatePaymentAsync(string transactionId)
        {
            var response = new BaseModel();
            var paymentId = await _paymentRepository.GetPaymentId(transactionId);     
            if (paymentId != 0)
            {
                response.Errors.Add(ErrorConstants.ImpossibleToCreatePayment);
                return response;
            }
            var payment = new Payment()
            {
                TransactionId = transactionId,
                CreationDate = DateTime.Now
            };
            var result = await _paymentRepository.CreateAsync(payment);
            if (! result)
            {
                response.Errors.Add(ErrorConstants.ImpossibleToCreatePayment);
            }
            return response;
        }

        private async Task<BaseModel> CreateOrderItemsAsync(List<OrderItemModelItem> model, long orderId)
        {
            var response = new BaseModel();
            var items = new List<OrderItem>();
            foreach (var item in model)
            {               
                var orderItem = OrderItemMapper.MapModelToEntity(item);
                orderItem.OrderId = orderId;
                items.Add(orderItem);
            }
            var result = await _orderItemRepository.CreateRangeAsync(items);
            if(!result)
            {
                response.Errors.Add(ErrorConstants.ImpossibleToCreateOrderItem);
            }
            return response;
        }

        private async Task<long> GetOrderIdAsync(long paymentId)
        {
            var id = await _orderRepository.GetOrderIdAsync(paymentId);
            return id;
        }

        private async Task<long> GetPaymentIdAsync(string transactionId)
        {
            var id = await _paymentRepository.GetPaymentId(transactionId);
            return id;
        }

        public async Task<BaseModel> MakeOrderAsync(CartModelItem model, long userId)
        {
            var result = await CreatePaymentAsync(model.TransactionId);
            if (result.Errors.Any())
            {
                return result;
            }
            var paymentId = await GetPaymentIdAsync(model.TransactionId);
            var order = OrderMapper.MapCartModelToModelItem(model, userId, paymentId);
            result = await CreateOrderAsync(order);
            if (result.Errors.Any())
            {
                return result;
            }
            var orderId = await GetOrderIdAsync(paymentId);
            result = await CreateOrderItemsAsync(model.OrderItems, orderId);
            if (result.Errors.Any())
            {
                return result;
            }
            var response = new OrdersModelItem();
            response.Id = orderId;
            return response;
        }

        public async Task<OrdersModel> GetAllOrdersAsync(OrdersBllFilterModel model)
        {
            var response = new OrdersModel();
            if (model == null)
            {
                response.Errors.Add(ErrorConstants.ModelIsNull);
                return response;
            }
            var filterModel = FilterMapper.MapOrdersFilteringModel(model);
            var orders = await _orderRepository.GetAllOrdersAsync(filterModel);
            if (orders == null)
            {
                response.Errors.Add(ErrorConstants.ImpossibleToFindOrder);
                return response;
            }
            var responceOrders = new OrdersModel();
            foreach (var order in orders.Data)
            {
                var orderItemModel = OrderMapper.MapResponseModelToModelItem(order);                
                responceOrders.Items.Add(orderItemModel);
            }
            responceOrders.TotalCount = orders.Data.Count();
            return responceOrders;
        }

        public async Task<BaseModel> DeleteOrderAsync(long id)
        {
            var response = new BaseModel();
            var order = await _orderRepository.GetByIdAsync(id);
            if (order == null)
            {
                response.Errors.Add(ErrorConstants.ImpossibleToFindOrder);
                return response;
            }
            var result = await _orderRepository.DeleteAsync(order);
            if (!result)
            {
                response.Errors.Add(ErrorConstants.ImpossibleToDeleteOrder);
            }
            return response;
        }

        public async Task<OrdersModel> GetUserOrdersAsync(long id)
        {
            var response = new OrdersModel();
            if (id == 0)
            {
                response.Errors.Add(ErrorConstants.ModelIsNull);
                return response;
            }
            var orders = await _orderRepository.GetUserOrdersAsync(id);
            if (orders == null)
            {
                response.Errors.Add(ErrorConstants.ImpossibleToFindOrder);
                return response;
            }
            var responceOrders = new OrdersModel();
            foreach (var order in orders)
            {
                var orderItemModel = OrderMapper.MapResponseModelToModelItem(order);
                responceOrders.Items.Add(orderItemModel);
            }
            responceOrders.TotalCount = orders.Count();
            return responceOrders;
        }
    }
}
