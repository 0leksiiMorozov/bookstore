﻿using Store.BusinessLogicLayer.Models.Base;
using static Store.BusinessLogicLayer.Models.Enums.Filter.Enums;

namespace Store.BusinessLogicLayer.Models.Orders
{
    public class OrdersBllFilterModel : BaseFilterModel
    {
        public FilterOrderByColumn ColumnSort { get; set; }
    }
}
