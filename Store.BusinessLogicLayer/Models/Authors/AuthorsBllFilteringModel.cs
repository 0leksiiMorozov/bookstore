﻿using Store.BusinessLogicLayer.Models.Base;
using static Store.BusinessLogicLayer.Models.Enums.Filter.Enums;

namespace Store.BusinessLogicLayer.Models.Authors
{
    public class AuthorsBllFilteringModel : BaseFilterModel
    {
        public FilterAuthorByColumn ColumnSort { get; set; }
    }
}
