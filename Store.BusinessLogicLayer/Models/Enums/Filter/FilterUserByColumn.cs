﻿namespace Store.BusinessLogicLayer.Models.Enums.Filter
{
    public partial class Enums
    {
        public enum FilterUserByColumn
        {
            Name = 0,
            Email = 1
        }
    }
}
