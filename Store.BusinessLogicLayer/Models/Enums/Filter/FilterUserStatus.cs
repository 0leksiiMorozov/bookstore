﻿namespace Store.BusinessLogicLayer.Models.Enums.Filter
{
    public partial class Enums
    {
        public enum FilterUserStatus
        {
            All = 0,
            Active = 1,
            Blocked = 2
        }
    }
}
