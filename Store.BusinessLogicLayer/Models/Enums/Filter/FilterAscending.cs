﻿namespace Store.BusinessLogicLayer.Models.Enums.Filter
{
    public partial class Enums
    {
        public enum FilterAscending
        {
            Ascending = 0,
            Descending = 1
        }
    }
}
