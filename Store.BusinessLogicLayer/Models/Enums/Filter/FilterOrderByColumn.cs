﻿namespace Store.BusinessLogicLayer.Models.Enums.Filter
{
    public partial class Enums
    {
        public enum FilterOrderByColumn
        {
            Id = 0,
            Date = 1,
            Amount = 2
        }
    }
}
