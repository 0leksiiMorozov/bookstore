﻿using Store.BusinessLogicLayer.Models.Base;

namespace Store.BusinessLogicLayer.Models.Users
{
    public class UserEditModel : BaseModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
