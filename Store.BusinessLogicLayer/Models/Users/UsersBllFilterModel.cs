﻿using Store.BusinessLogicLayer.Models.Base;
using static Store.BusinessLogicLayer.Models.Enums.Filter.Enums;

namespace Store.BusinessLogicLayer.Models.Users
{
    public class UsersBllFilterModel : BaseFilterModel
    {
        public FilterUserStatus BlockState { get; set; }
        public FilterUserByColumn ColumnSort { get; set; }
    }   
}
