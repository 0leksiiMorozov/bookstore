﻿using Store.BusinessLogicLayer.Models.Authors;
using Store.BusinessLogicLayer.Models.Orders;
using Store.BusinessLogicLayer.Models.PrintingEdition;
using Store.BusinessLogicLayer.Models.Users;
using Store.DataAccessLayer.Models.Filter;
using System.Linq;
using DataEnums = Store.DataAccessLayer.Common.Enums.Filter.Enums;
using DataTypeEnum = Store.DataAccessLayer.Common.Enums.Entity.Enums;


namespace Store.BusinessLogicLayer.Mappers
{
    public class FilterMapper
    {
        public static UserDalFilterModel MapUserFilteringModel(UsersBllFilterModel model)
        {
            var dalModel = new UserDalFilterModel()
            {
                BlockState = (DataEnums.FilterUserBlock)model.BlockState,
                AscendingSort = (DataEnums.AscendingSort)model.AscendingSort,
                ColumnSort = (DataEnums.FilterUserByColumn)model.ColumnSort,
                PageCount = model.PageCount,
                SearchString = model.SearchString,
                PageSize = model.PageSize
            };
            return dalModel;
        }

        public static AuthorsDalFilterModel MapAuthorsFilteringModel(AuthorsBllFilteringModel model)
        {
            var dalModel = new AuthorsDalFilterModel()
            {
                PageCount = model.PageCount,
                SearchString = model.SearchString,
                PageSize = model.PageSize,
                AscendingSort = (DataEnums.AscendingSort)model.AscendingSort,
                ColumnSort = (DataEnums.FilterAuthorByColumn)model.ColumnSort
            };
            return dalModel;
        }

        public static OrdersDalFilterModel MapOrdersFilteringModel(OrdersBllFilterModel model)
        {
            var dalModel = new OrdersDalFilterModel()
            {
                PageCount = model.PageCount,
                SearchString = model.SearchString,
                PageSize = model.PageSize,
                AscendingSort = (DataEnums.AscendingSort)model.AscendingSort,
                ColumnSort = (DataEnums.FilterOrdersByColumns)model.ColumnSort
            };
            return dalModel;
        }

        public static PrintingEditionsDalFilterModel MapPrintingEditionsFilteringModel(PrintingEditionsBllFilterModel model)
        {
            var dalModel = new PrintingEditionsDalFilterModel()
            {
                PageCount = model.PageCount,
                PageSize = model.PageSize,
                MinPrice = model.MinPrice,
                MaxPrice = model.MaxPrice,
                ProductType = model.ProductTypes.Cast<DataTypeEnum.ProductType>().ToList(),
                SearchString = model.SearchString,
                ColumnFilter = (DataEnums.FilterProductByColumns)model.ColumnFilter,
                AscendingSort = (DataEnums.AscendingSort)model.AscendingSort
            };
            return dalModel;
        }
    }
}
