﻿namespace Store.BusinessLogicLayer.Common.Constants.PasswordGenerator
{
    public partial class Constants
    {
        public class PasswordGeneration
        {
            public const int RequiredLength = 10;
            public const int UniqueChars = 4;
            public static readonly string[] AllChars = { "ABCDEFGHJKLMNOPQRSTUVWXYZ", "abcdefghijkmnopqrstuvwxyz", "0123456789", "!@$?_-" };
        }
    }
}
