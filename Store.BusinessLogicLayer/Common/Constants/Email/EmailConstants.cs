﻿namespace Store.BusinessLogicLayer.Common.Constants.Email
{
    public partial class Constants
    {
        public class EmailConstants
        {
            public const string EmailForSending = "sending.email.testing@gmail.com";
            public const string EmailPassword = "PassWord{}12";
            public const int SmtpPort = 587;
            public const string SmtpHost = "smtp.gmail.com";
            public const string ConfirmationSubject = "Confirm your e-mail!";
            public const string ResetPasswordSubject = "Reset Password!";
            public const string NewPasswordSubject = "Your new Password!";
        }
    }
}
