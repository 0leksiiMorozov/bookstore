﻿namespace Store.BusinessLogicLayer.Common.Constants.Jwt
{
    public partial class Constants
    {
        public class JwtConstants
        {
            public const string SigningSecurityKey = "0d5b3235a8b403c3dab9c3f4f65c07fcalskd234n141faza";
            public const string JwtSchemeName = "JwtBearer";
            public const string ValidIssuer = "BookStore";
            public const string ValidAudience = "BookStoreClient";
            public const string AccessToken = "AccessToken";
            public const string RefreshToken = "refreshtoken";

        }
    }
}
