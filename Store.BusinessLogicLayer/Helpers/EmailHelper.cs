﻿using Store.BusinessLogicLayer.Common.Constants.Email;
using Store.BusinessLogicLayer.Helpers.Interfaces;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Store.BusinessLogicLayer.Helpers
{
    public class EmailHelper : IEmailHelper
    {
        public async Task<bool> SendMessageAsync(string sendTo, string body, string subject)
        {
            var mailMessage = new MailMessage();           
            mailMessage.From = new MailAddress(Constants.EmailConstants.EmailForSending);
            mailMessage.To.Add(new MailAddress(sendTo));
            mailMessage.Subject = subject;
            mailMessage.Body = body;

            using (var smtpClient = new SmtpClient())
            {
                smtpClient.Host = Constants.EmailConstants.SmtpHost;
                smtpClient.Port = Constants.EmailConstants.SmtpPort;
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(Constants.EmailConstants.EmailForSending, Constants.EmailConstants.EmailPassword);

                await smtpClient.SendMailAsync(mailMessage);
            }
            return true;
        }
    }
}
