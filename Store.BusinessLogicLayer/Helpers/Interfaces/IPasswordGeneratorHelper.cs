﻿namespace Store.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IPasswordGeneratorHelper
    {
        public string GenerateRandomPassword();
    }
}
