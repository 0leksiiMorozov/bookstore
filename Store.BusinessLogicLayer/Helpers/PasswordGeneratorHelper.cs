﻿using Microsoft.AspNetCore.Identity;
using Store.BusinessLogicLayer.Helpers.Interfaces;
using static Store.BusinessLogicLayer.Common.Constants.PasswordGenerator.Constants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Store.BusinessLogicLayer.Helpers
{
    public class PasswordGeneratorHelper : IPasswordGeneratorHelper
    {
        public string GenerateRandomPassword()
        {
            var opts = new PasswordOptions()
            {
                RequiredLength = PasswordGeneration.RequiredLength,
                RequiredUniqueChars = PasswordGeneration.UniqueChars,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
            {
                chars.Insert(rand.Next(0, chars.Count),
                PasswordGeneration.AllChars[0][rand.Next(0, PasswordGeneration.AllChars[0].Length)]);
            }
            if (opts.RequireLowercase)
            {
                chars.Insert(rand.Next(0, chars.Count),
                PasswordGeneration.AllChars[1][rand.Next(0, PasswordGeneration.AllChars[1].Length)]);
            }
            if (opts.RequireDigit)
            {
                chars.Insert(rand.Next(0, chars.Count),
                PasswordGeneration.AllChars[2][rand.Next(0, PasswordGeneration.AllChars[2].Length)]);
            }
            if (opts.RequireNonAlphanumeric)
            {
                chars.Insert(rand.Next(0, chars.Count),
                PasswordGeneration.AllChars[3][rand.Next(0, PasswordGeneration.AllChars[3].Length)]);
            }
            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = PasswordGeneration.AllChars[rand.Next(0, PasswordGeneration.AllChars.Length)];
                chars.Insert(rand.Next(0, chars.Count), rcs[rand.Next(0, rcs.Length)]);
            }
            return new string(chars.ToArray());
        }
    }
}
