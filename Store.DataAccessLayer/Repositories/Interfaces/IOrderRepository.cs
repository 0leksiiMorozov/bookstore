﻿using Store.DataAccessLayer.Models.Filter;
using Store.DataAccessLayer.Models.Response;
using Store.DataAccessLayer.Repositories.EFRepositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Store.DataAccessLayer.Repositories.Interfaces
{
    public  interface IOrderRepository : IBaseEFRepository<Order>
    {
        Task<List<OrderResponseModel>> GetUserOrdersAsync(long id);
        Task<ResponseModel<List<OrderResponseModel>>> GetAllOrdersAsync(OrdersDalFilterModel model);
        Task<long> GetOrderIdAsync(long paymentId);
    }
}
