﻿using Store.DataAccessLayer.Models.Filter;
using Store.DataAccessLayer.Models.Response;
using Store.DataAccessLayer.Repositories.EFRepositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Store.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorInPrintingEditionRepository : IBaseEFRepository<AuthorInPrintingEdition>
    {
        Task<ResponseModel<List<PrintingEditionsResponseModel>>> GetAllPrintingEditionsAsync(PrintingEditionsDalFilterModel model);
        Task<List<AuthorInPrintingEdition>> GetAuthorInProductByProductId(long id);
    }
}
