﻿using System;
using System.Linq;
using System.Linq.Expressions;
using static Store.DataAccessLayer.Common.Enums.Filter.Enums;

namespace Store.DataAccessLayer.Common.Extensions
{
    public static class OrderFilteringExtension
    {
        public static IQueryable<TSource> SortOrder<TSource, TKey>(
            this IQueryable<TSource> source,
            Expression<Func<TSource, TKey>> keySelector,
            AscendingSort order)
        {
            if (order == AscendingSort.Ascending)
            {
                return source.OrderBy(keySelector);
            }
            return source.OrderByDescending(keySelector);
        }
    }
}
