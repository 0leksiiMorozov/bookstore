﻿namespace Store.DataAccessLayer.Common.Enums.Filter
{
    public partial class Enums
    {
        public enum AscendingSort
        {
            Ascending = 0,
            Descending = 1
        }
    }
}
