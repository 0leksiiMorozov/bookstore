﻿using static Store.DataAccessLayer.Common.Enums.Filter.Enums;

namespace Store.DataAccessLayer.Models.Filter
{
    public class AuthorsDalFilterModel : BaseDalFilterModel
    {
        public FilterAuthorByColumn ColumnSort { get; set; }
    }
}
