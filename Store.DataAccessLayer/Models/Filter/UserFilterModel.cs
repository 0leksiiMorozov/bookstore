﻿using static Store.DataAccessLayer.Common.Enums.Filter.Enums;

namespace Store.DataAccessLayer.Models.Filter
{
    public class UserDalFilterModel : BaseDalFilterModel
    {
        public FilterUserBlock BlockState { get; set; }
        public FilterUserByColumn ColumnSort { get; set; }
    }
}
