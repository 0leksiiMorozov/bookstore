﻿using static Store.DataAccessLayer.Common.Enums.Filter.Enums;

namespace Store.DataAccessLayer.Models.Filter
{
    public class OrdersDalFilterModel : BaseDalFilterModel
    {
        public FilterOrdersByColumns ColumnSort { get; set; }
    }
}
