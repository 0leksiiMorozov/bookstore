﻿using Store.DataAccessLayer.Repositories.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Store.DataAccessLayer.Repositories.EFRepositories
{
    public class AuthorInPrintingEdition : BaseEntity
    {

        public long AuthorId { get; set; }

        [ForeignKey("AuthorId")]
        public Author Author { get; set; }

        
        public long PrintingEditionId { get; set; }

        [ForeignKey("PrintingEditionId")]
        public PrintingEdition PrintingEdition { get; set; }
    }
}
