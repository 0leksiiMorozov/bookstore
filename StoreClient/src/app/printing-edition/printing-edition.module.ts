import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

import { PrintingEditionRoutingModule } from 'src/app/printing-edition/printing-edition-routing.module';

import { GetAllPrintingEditionsComponent, AddPrintingEditionComponent, PrintingEditionDetailsComponent, HomeComponent } from 'src/app';

import { routes } from 'src/app/printing-edition/printing-edition-routing.module';



@NgModule({
  declarations: [
    GetAllPrintingEditionsComponent,
    AddPrintingEditionComponent,
    PrintingEditionDetailsComponent,
    HomeComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    PrintingEditionRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class PrintingEditionModule { }
