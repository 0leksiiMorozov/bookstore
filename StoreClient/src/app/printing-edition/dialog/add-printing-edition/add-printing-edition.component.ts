import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AuthorModel, PrintingEditionDialogModel, AuthorModelItem } from 'src/app/shared/models';
import { PrintingEditionService } from 'src/app/shared/services/printing-edition.service';
import { ProductType, Currency  } from 'src/app/shared/enums';
import { AuthorService } from 'src/app/shared/services/author.service';
import { EntityConstants, DialogConstants } from 'src/app/shared/constants';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-printing-edition',
  templateUrl: './add-printing-edition.component.html',
  styleUrls: ['./add-printing-edition.component.css'],
  providers: [PrintingEditionService]
})
export class AddPrintingEditionComponent implements OnInit {
  authors: Array<AuthorModelItem>;
  productTypes: string[];
  currency: string[];
  productForm: FormGroup;
  selectedProduct: string;
  selectedAuthorsControl: FormControl;

  constructor(
    private formBuilder: FormBuilder,
    private printingEditionService: PrintingEditionService,
    private authorService: AuthorService,
    private dialogRef: MatDialogRef<AddPrintingEditionComponent>,
    @Inject(MAT_DIALOG_DATA) public product: PrintingEditionDialogModel
  ) {
    this.authors = new Array<AuthorModelItem>();
    this.productTypes = EntityConstants.productTypes;
    this.currency = EntityConstants.currency;

    this.productForm = this.formBuilder.group({
      title: [this.product.printingEdition.title],
      description: [this.product.printingEdition.description],
      productType: [ProductType[this.product.printingEdition.productType]],
      currency: [Currency[this.product.printingEdition.currency]],
      price: [this.product.printingEdition.price]
    });
    this.selectedAuthorsControl = new FormControl();
  }

  ngOnInit(): void {
    this.getAuthors();
  }

  cancelButton(): void {
    this.dialogRef.close();
  }

  chooseMethod(): void {
    this.product.printingEdition.description = this.productForm.controls.description.value;
    this.product.printingEdition.title = this.productForm.controls.title.value;
    this.product.printingEdition.price = this.productForm.controls.price.value;
    this.product.printingEdition.authors = this.selectedAuthorsControl.value;
    if ( this.product.actionString === DialogConstants.createActionString) {
      this.createPrintingEdition();
      return;
    }
    this.updatePrintingEdition();
  }

  createPrintingEdition(): void {
    this.printingEditionService.createPrintingEdition(this.product.printingEdition).subscribe();
  }

  updatePrintingEdition(): void {
    this.printingEditionService.updatePrintingEdition(this.product.printingEdition).subscribe();
  }

  getAuthors(): void {
    this.authorService.getAuthors().subscribe(data => {
      this.authors = data.items;
      if (this.product.actionString === DialogConstants.updateActionString) {
        this.selectedAuthorsControl = new FormControl(this.getSelectedAuthors());
      }
    });
  }

  setProductType(type: string): void {
    this.product.printingEdition.productType = ProductType[type];
  }

  setCurrency(element: string): void {
    this.product.printingEdition.currency = Currency[element];
  }

  getSelectedAuthors(): Array<AuthorModelItem> {
    let selectedAuthors = new Array<AuthorModelItem>();
    if (this.product !== null) {
      this.product.printingEdition.authors.forEach(element => {
        this.authors.forEach(author => {
          if (author.id === element.id ) {
            selectedAuthors.push(author);
          }
        });
      });
    }
    return selectedAuthors;
  }
}
