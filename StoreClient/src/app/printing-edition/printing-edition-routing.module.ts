import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GetAllPrintingEditionsComponent, AddPrintingEditionComponent, PrintingEditionDetailsComponent, HomeComponent } from 'src/app';

export const routes: Routes = [
  {path: 'getAllPrintingEditions', component: GetAllPrintingEditionsComponent},
  {path: 'addPrintingEdition', component: AddPrintingEditionComponent},
  {path: 'home', component: HomeComponent},
  {path: 'productDetails/:id', component: PrintingEditionDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintingEditionRoutingModule { }
