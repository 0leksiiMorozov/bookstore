import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { PrintingEditionModel, PrintingEditionModelItem, AuthorModelItem, FilterPrintingEditionModel, UserModelItem } from 'src/app/shared/models';
import { FilterConstants, EntityConstants } from 'src/app/shared/constants';
import { Currency, ProductType, FilterProductByColumn, FilterAscending } from 'src/app/shared/enums';

import { PrintingEditionService } from 'src/app/shared/services/printing-edition.service';

import { environment } from 'src/environments/environment';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser: UserModelItem;
  printingEditions: PrintingEditionModel;
  filterModel: FilterPrintingEditionModel;
  totalProductCount: number;
  filterForm: FormGroup;
  types: string[];
  productTypes: FormControl;
  defaultCurrency: Currency;
  booksOnPage: PrintingEditionModelItem[];
  currency: string[];
  filterPriceDirection: string[];
  pageSizeOptions: number[];
  minPrice: number;
  maxPrice: number;
  priceStep: number;
  imagesPath: string;

  constructor(
    private localStorageService: LocalStorageService,
    private printingEditionService: PrintingEditionService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.currentUser = new UserModelItem();
    this.defaultCurrency = Currency.USD;
    this.imagesPath = environment.imagesPath;

    iconRegistry.addSvgIcon('mainBook', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/mainBook.svg`))
      .addSvgIcon('search', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/search.svg`))
      .addSvgIcon('settings', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/settings.svg`));

    this.filterModel = new FilterPrintingEditionModel();
    this.printingEditions = new PrintingEditionModel();

    this.types = EntityConstants.productTypes;
    this.currency = EntityConstants.currency;
    this.filterPriceDirection = FilterConstants.priceDirection;
    this.pageSizeOptions = EntityConstants.mainPageSizeOptions;

    this.productTypes = new FormControl(this.types);
    this.filterForm = this.formBuilder.group({
      searchString: [FilterConstants.emptyLine, Validators.minLength(3)],
      columnFilter: [FilterProductByColumn.price],
      currency: [this.defaultCurrency],
      ascendingSort: [FilterAscending.asc],
      pageSize: [FilterConstants.mainPageSize],
      pageCount: [FilterConstants.pageCount]
    });

    this.booksOnPage = [];
    this.minPrice = FilterConstants.minPrice;
    this.maxPrice = FilterConstants.maxPrice;
    this.priceStep = FilterConstants.priceStep;
   }

  ngOnInit() {
    this.localStorageService.user.subscribe(data => {
      this.currentUser = data;
    });
    this.getAllPrintingEditions();
  }

  isRoleAdmin(): boolean {
    return this.currentUser.role === 'Admin' ? true : false;
  }

  isLoggedIn(): boolean {
    return this.localStorageService.isLoggedIn();
  }

  getAllPrintingEditions(): void {
    this.filterModel = this.filterForm.value;
    this.setProductType(this.productTypes.value);
    this.filterModel.minPrice = this.minPrice;
    this.filterModel.maxPrice = this.maxPrice;
    this.printingEditionService.getAllPrintingEditions(this.filterModel).subscribe(data => {
      this.printingEditions = data;
      this.totalProductCount = data.totalCount;
      this.setBookOnPage();
    });
  }

  searchStringMinLengthCheck(): void {
    if (this.filterForm.controls.searchString.value.length >= 3 ||
      this.filterForm.controls.searchString.value.length === 0) {
      this.getAllPrintingEditions();
    }
  }

  setBookOnPage(): void {
    this.booksOnPage.splice(0);
    this.printingEditions.items.forEach(element => {
      this.booksOnPage.push(element);
    });
  }

  displayAuthors(authors: Array<AuthorModelItem>): string[] {
    const authorNames = [];
    authors.forEach(x => {
      authorNames.push(x.name);
    });
    return authorNames;
  }

  setProductType(productTypes: string[]): void {
    const types: Array<ProductType> = new Array<ProductType>();
    productTypes.forEach(element => {
    types.push(ProductType[element]);
    });
    this.filterModel.productTypes = types;
  }

  setCurrency(element: string): void {
    this.filterForm.controls.currency.setValue(Currency[element]);
    this.getAllPrintingEditions();
  }

  setPaginationOptions($event): void {
    this.filterForm.controls.pageCount.setValue($event.pageIndex + 1);
    this.filterForm.controls.pageSize.setValue($event.pageSize);

    this.getAllPrintingEditions();
  }

  setDirectionSort(direction: string): void {
    direction === FilterConstants.priceDirection[0] ?
    this.filterForm.controls.ascendingSort.setValue(FilterAscending.desc) :
    this.filterForm.controls.ascendingSort.setValue(FilterAscending.asc);
    this.filterForm.controls.columnFilter.setValue(FilterProductByColumn.price);
    this.getAllPrintingEditions();
  }

  getPrice(price: number, currencyType: string): string {
    let currencyString;
    currencyString = Currency[currencyType];
    return `Price: ${price} ${currencyString}`;
  }

  getProductDetails(productId: number): void {
    this.router.navigate([`/printingEdition/productDetails/${productId}`]);
  }
}
