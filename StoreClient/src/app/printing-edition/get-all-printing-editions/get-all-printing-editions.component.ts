import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { MatDialog, MatIconRegistry } from '@angular/material';

import { PrintingEditionModelItem, PrintingEditionModel, FilterPrintingEditionModel, AuthorModel, UserModelItem } from 'src/app/shared/models';
import { FilterConstants, EntityConstants } from 'src/app/shared/constants';
import { FilterAscending, ProductType, FilterProductByColumn, FilterOrderByColumn } from 'src/app/shared/enums';

import { PrintingEditionService } from 'src/app/shared/services/printing-edition.service';
import { AddPrintingEditionComponent } from 'src/app/printing-edition/dialog/add-printing-edition/add-printing-edition.component';
import { DeleteComponent } from 'src/app/shared/components/delete/delete.component';

import { environment } from 'src/environments/environment';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-get-all-printing-editions',
  templateUrl: './get-all-printing-editions.component.html',
  styleUrls: ['./get-all-printing-editions.component.css'],
  providers: [PrintingEditionService]
})
export class GetAllPrintingEditionsComponent implements OnInit {
  currentUser: UserModelItem;
  printingEditions: PrintingEditionModel;
  filterModel: FilterPrintingEditionModel;
  authors: AuthorModel;
  filterForm: FormGroup;
  productType: FormControl;
  tableColumns: string[];
  pageSizeOptions: number[];
  totalProductsCount: number;
  productTypes: string[];
  imagePath: string;

  constructor(
    private localStorageService: LocalStorageService,
    private printingEditionService: PrintingEditionService,
    private dialog: MatDialog,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private router: Router,
    private formBuilder: FormBuilder
    ) {
      this.currentUser = new UserModelItem();
      this.imagePath = environment.imagesPath;

      iconRegistry.addSvgIcon(
        'pencil', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagePath}/pencil.svg`)).addSvgIcon(
        'delete', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagePath}/delete.svg`)).addSvgIcon(
        'search', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagePath}/search.svg`)).addSvgIcon(
        'add', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagePath}/add.svg`));

      this.printingEditions = new PrintingEditionModel();
      this.filterModel = new FilterPrintingEditionModel();
      this.authors = new AuthorModel();

      this.tableColumns = EntityConstants.printingEditionTableColumns;
      this.pageSizeOptions = EntityConstants.pageSizeOptions;
      this.productTypes = EntityConstants.productTypes;

      this.filterForm = this.formBuilder.group({
      searchString: [FilterConstants.emptyLine, Validators.minLength(3)],
      columnFilter: [FilterOrderByColumn.id],
      ascendingSort: [FilterAscending.asc],
      pageSize: [FilterConstants.pageSize],
      pageCount: [FilterConstants.pageCount]
      });

      this.productType = new FormControl(this.productTypes);
    }

  ngOnInit(): void {
    this.localStorageService.user.subscribe(data => {
      this.currentUser = data;
    });
    this.getAllPrintingEditions();

  }

  isRoleAdmin(): boolean {
    return this.currentUser.role === 'Admin' ? true : false;
  }

  private getAllPrintingEditions(): void {
    this.filterModel = this.filterForm.value;
    this.setProductType(this.productType.value);
    this.printingEditionService.getAllPrintingEditions(this.filterModel).subscribe(data => {
      this.totalProductsCount = data.totalCount;
      this.printingEditions = data;
    });
  }

  searchStringMinCharactersCheck(): void {
    if (this.filterForm.controls.searchString.value.length >= 3 ||
      this.filterForm.controls.searchString.value.length === 0) {
      this.getAllPrintingEditions();
    }
  }

  getProductType(type: number): string {
    return ProductType[type];
  }

  private setProductType(productTypes: string[]): void {
    const types = new Array<ProductType>();

    productTypes.forEach(element => {
      types.push(ProductType[element]);
    });
    this.filterModel.productTypes = types;
  }

  setAscendingSort(direction: string, column: string): void {
    this.filterForm.controls.ascendingSort.setValue(FilterAscending[direction]);

    this.filterForm.controls.columnFilter.setValue(FilterProductByColumn[column]);

    this.getAllPrintingEditions();
  }

  openCreateProductDialog(): void {
    let printingEdition = new PrintingEditionModelItem();
    let actionString = 'Create';
    const dialogRef = this.dialog.open(AddPrintingEditionComponent, {
      width: '800px',
      data: {printingEdition, actionString}
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getAllPrintingEditions();
    });
  }

  openUpdateProductDialog(printingEdition: PrintingEditionModelItem): void {
    let actionString = 'Update';
    const dialogRef = this.dialog.open(AddPrintingEditionComponent, {
      width: '800px',
      data: {printingEdition, actionString}
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getAllPrintingEditions();
    });
  }

  openDeleteProductDialog(id: number): void {
    let nameEntityToDelete = 'Printing Edition';
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '400px',
      data: {nameEntityToDelete}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data === true) {
        this.printingEditionService.deletePrintingEdition(id);
      }
      this.getAllPrintingEditions();
    });
  }

  setPaginationOptions($event): void {
    this.filterForm.controls.pageCount.setValue($event.pageIndex + 1);
    this.filterForm.controls.pageSize.setValue($event.pageSize);

    this.getAllPrintingEditions();
  }
}
