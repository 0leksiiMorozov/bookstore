export * from 'src/app/user/dialogs/edit-user-profile/edit-user-profile.component';
export * from 'src/app/user/get-all-users/get-all-users.component';
export * from 'src/app/user/profile/profile.component';
export * from 'src/app/account/confirm-email/confirm-email.component';
export * from 'src/app/account/log-in/log-in.component';
export * from 'src/app/account/registration/registration.component';
export * from 'src/app/account/reset-password/reset-password.component';
export * from 'src/app/author/dialog/add-author/add-author.component';
export * from 'src/app/author/get-all-authors/get-all-authors.component';
export * from 'src/app/order/user-orders/user-orders.component';
export * from 'src/app/order/get-all-orders/get-all-orders.component';
export * from 'src/app/printing-edition/dialog/add-printing-edition/add-printing-edition.component';
export * from 'src/app/printing-edition/get-all-printing-editions/get-all-printing-editions.component';
export * from 'src/app/printing-edition/home/home.component';
export * from 'src/app/printing-edition/printing-edition-details/printing-edition-details.component';
export * from 'src/app/shared/components/footer/footer.component';
export * from 'src/app/shared/components/delete/delete.component';

export class Index {
}
