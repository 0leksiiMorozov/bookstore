import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material/material.module';

import { AdministratorRoutingModule } from 'src/app/administrator/administrator-routing.module';
import { RouterModule } from '@angular/router';
import { routes } from 'src/app/administrator/administrator-routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    AdministratorRoutingModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ]
})
export class AdministratorModule { }
