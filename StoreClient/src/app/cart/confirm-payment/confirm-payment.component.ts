import { Component, OnInit, Predicate } from '@angular/core';
import { MatDialogRef, MatDialog, MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

import { CartModelItem, OrderItemModelItem } from 'src/app/shared/models';
import { LocalStorageService, PaymentService, CartService } from 'src/app/shared/services';
import { EntityConstants } from 'src/app/shared/constants';

import { environment } from 'src/environments/environment';
import { PaymentSuccessComponent } from 'src/app/cart/payment-success/payment-success.component';

@Component({
  selector: 'app-confirm-payment',
  templateUrl: './confirm-payment.component.html',
  styleUrls: ['./confirm-payment.component.css']
})
export class ConfirmPaymentComponent implements OnInit {
  cartModel: CartModelItem;
  tableColumns: string[];
  handler: any = null;
  transactionInfo: any;
  orderId: number;
  imagePath: string;
  paymentCallBack: Predicate<string>;

  constructor(
    private paymentService: PaymentService,
    private cartService: CartService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private localStorageService: LocalStorageService,
    public dialogRef: MatDialogRef<ConfirmPaymentComponent>,
    ) {
      this.cartModel = new CartModelItem();
      this.tableColumns = EntityConstants.cartTableColumns;
      this.transactionInfo = null;
      this.orderId = 0;
      this.imagePath = environment.imagesPath;

      iconRegistry.addSvgIcon('delete', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagePath}/delete.svg`));
    }

  ngOnInit(): void {
    this.localStorageService.cart.subscribe(data => {
      this.cartModel = data;
    });
    this.paymentCallBack = this.makeOrder.bind(this);
  }

  countUnitPrice(orderItemModel: OrderItemModelItem): string {
    let unitPrice = orderItemModel.amount / orderItemModel.count;
    return `$ ${unitPrice}`;
  }

  countTotalAmount(): number {
    let totalAmount = 0;
    this.cartModel.orderItems.forEach(x => {
      totalAmount += x.amount;
    });
    this.cartModel.totalAmount = totalAmount;
    return totalAmount;
  }

  cancelButton(): void {
    this.dialogRef.close();
  }

  deleteOrderItem(product: OrderItemModelItem): void {
    debugger
    let result = this.localStorageService.removeItemFromCart(product);
    if (result) {
      this.cancelButton();
    }
  }

  makeOrder(transactionId: string): void {
    this.cartModel.transactionId = transactionId;
    this.cartService.createOrder(this.cartModel).subscribe(data => {
      this.orderId = data.id;
    });
    this.cancelButton();
    this.openPaymentSuccessDialog(this.orderId);
  }

  buy(): void {
    this.paymentService.pay(this.countTotalAmount(), this.paymentCallBack);
  }

  openPaymentSuccessDialog(orderId: number): void {
    const dialogRef = this.dialog.open(PaymentSuccessComponent, {
      width: '400px',
      data: {orderId}
    });
    dialogRef.afterClosed().subscribe(() => {
        this.localStorageService.removeCart();
    });
  }
}
