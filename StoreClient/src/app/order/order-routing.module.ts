import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GetAllOrdersComponent, UserOrdersComponent } from 'src/app';

export const routes: Routes = [
  {path: 'getAllOrders', component: GetAllOrdersComponent},
  {path: 'userOrders', component: UserOrdersComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
