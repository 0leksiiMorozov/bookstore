import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { ProductType, OrderStatus, FilterOrderByColumn, FilterAscending } from 'src/app/shared/enums';
import { OrderService } from 'src/app/shared/services/order.service';
import { OrderModel, FilterOrderModel } from 'src/app/shared/models';
import { EntityConstants, FilterConstants } from 'src/app/shared/constants';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-user-orders',
  templateUrl: './user-orders.component.html',
  styleUrls: ['./user-orders.component.css']
})
export class UserOrdersComponent implements OnInit {
  orders: OrderModel;
  userId: number;
  filterForm: FormGroup;
  tableColumns: string[];
  pageSizeOptions: number[];
  totalOrdersCount: number;
  orderStatus: string[];
  filterModel: FilterOrderModel;

  constructor(
    private formBuilder: FormBuilder,
    private orderService: OrderService,
    private localStorageService: LocalStorageService,
  ) {
    this.orders = new OrderModel();

    this.filterForm = this.formBuilder.group({
      searchString: [FilterConstants.emptyLine],
      columnSort: [FilterOrderByColumn.id],
      ascendingSort: [FilterAscending.asc],
      pageSize: [FilterConstants.pageSize],
      pageCount: [FilterConstants.pageCount]
      });

    this.tableColumns = EntityConstants.userOrderTableColumn;
    this.pageSizeOptions = EntityConstants.pageSizeOptions;
    this.orderStatus = EntityConstants.orderStatus;
    this.filterModel = new FilterOrderModel();
   }

  ngOnInit(): void {
    this.getUserId();
    this.getUserOrders();
  }

  getUserId(): void {
    let id = this.localStorageService.getUser().id;
    this.userId = id;
  }

  getProductType(type: number): string {
    return ProductType[type];
  }

  getStatusType(type: number): string {
    return OrderStatus[type];
  }

  getUserOrders(): void {
    this.orderService.getUserOrders(this.userId).subscribe( data => {
      this.orders = data;
      this.totalOrdersCount = data.totalCount;
    });
  }

  setPaginationOptions($event): void {
    this.filterForm.controls.pageCount.setValue($event.pageIndex + 1);
    this.filterForm.controls.pageSize.setValue($event.pageSize);

    this.getUserOrders();
  }
}
