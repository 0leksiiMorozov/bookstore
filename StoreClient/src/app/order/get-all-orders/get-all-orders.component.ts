import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { FilterAscending, ProductType, OrderStatus, FilterOrderByColumn } from 'src/app/shared/enums';
import { OrderModel, FilterOrderModel } from 'src/app/shared/models';
import { OrderService } from 'src/app/shared/services/order.service';
import { EntityConstants, FilterConstants } from 'src/app/shared/constants';
import { environment } from 'src/environments/environment';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';


@Component({
  selector: 'app-get-all-orders',
  templateUrl: './get-all-orders.component.html',
  styleUrls: ['./get-all-orders.component.css'],
  providers: [OrderService]
})
export class GetAllOrdersComponent implements OnInit {
  orders: OrderModel;
  filterModel: FilterOrderModel;
  tableColumns: string[];
  pageSizeOptions: number[];
  totalOrdersCount: number;
  filterForm: FormGroup;
  orderStatus: string[];
  currentStatus: FormControl;
  imagesPath: string;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private orderService: OrderService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer
    ) {
      this.imagesPath = environment.imagesPath;

      iconRegistry
        .addSvgIcon('search', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/search.svg`));

      this.orders = new OrderModel();
      this.filterModel = new FilterOrderModel();
      this.tableColumns = EntityConstants.orderTableColumns;
      this.pageSizeOptions = EntityConstants.pageSizeOptions;
      this.orderStatus = EntityConstants.orderStatus;

      this.filterForm = this.formBuilder.group({
      searchString: [FilterConstants.emptyLine],
      columnSort: [FilterOrderByColumn.id],
      ascendingSort: [FilterAscending.asc],
      pageSize: [FilterConstants.pageSize],
      pageCount: [FilterConstants.pageCount]
      });
      this.currentStatus = new FormControl(this.orderStatus);
      }

  ngOnInit() {
    if (!this.localStorageService.checkRole('Admin')) {
      this.router.navigate(['/printingEdition/home']);
    }
    this.getAllOrders();
  }

  getProductType(type: number): string {
    return ProductType[type];
  }

  getStatusType(type: number): string {
    return OrderStatus[type];
  }

  getAllOrders(): void {
    this.filterModel = this.filterForm.value;
    this.filterModel.orderStatus = OrderStatus.Paid;
    this.orderService.getAllOrders(this.filterModel).subscribe(data => {
      this.orders = data;
      this.totalOrdersCount = data.totalCount;
    });
  }

  setPaginationOptions($event): void {
    this.filterForm.controls.pageCount.setValue($event.pageIndex + 1);
    this.filterForm.controls.pageSize.setValue($event.pageSize);
    this.getAllOrders();
  }

  setAscendingSort(direction: string, column: string): void {
    const sortDirection: FilterAscending = FilterAscending[direction];
    this.filterForm.controls.ascendingSort.setValue(sortDirection);

    const sortColumn: FilterOrderByColumn = FilterOrderByColumn[column];
    this.filterForm.controls.columnSort.setValue(sortColumn);

    this.getAllOrders();
  }
}
