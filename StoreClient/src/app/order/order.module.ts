import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from 'src/app/material/material.module';
import { OrderRoutingModule } from 'src/app/order/order-routing.module';

import { GetAllOrdersComponent, UserOrdersComponent } from 'src/app';

import { routes } from 'src/app/order/order-routing.module';

@NgModule({
  declarations: [
    GetAllOrdersComponent,
    UserOrdersComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ]
})
export class OrderModule { }
