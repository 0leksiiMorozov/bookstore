export enum FilterUserStatus {
  All = 0,
  Active = 1,
  Blocked = 2
}
