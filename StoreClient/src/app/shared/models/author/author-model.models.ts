import { AuthorModelItem } from 'src/app/shared/models';

export class AuthorModel {
  public items: Array<AuthorModelItem>;
  public totalCount: number;
}
