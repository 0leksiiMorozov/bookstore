import { AuthorModelItem } from 'src/app/shared/models';

export class AuthorDialogModel {
  public author: AuthorModelItem;
  public actionString: string;
}
