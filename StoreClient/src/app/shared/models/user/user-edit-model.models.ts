export class UserEditModel {
  public id: number;
  public firstName?: string;
  public lastName?: string;
  public email?: string;
  public oldPassword?: string;
  public newPassword?: string;
  public role: string;
}
