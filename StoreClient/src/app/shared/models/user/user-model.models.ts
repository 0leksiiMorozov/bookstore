import { UserModelItem } from 'src/app/shared/models';

export class UserModel {
  items: Array<UserModelItem>;
  totalCount: number;
}
