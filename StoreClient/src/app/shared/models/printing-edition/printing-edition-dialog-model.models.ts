import { PrintingEditionModelItem } from 'src/app/shared/models';

export class PrintingEditionDialogModel {
  public printingEdition: PrintingEditionModelItem;
  public actionString: string;
}
