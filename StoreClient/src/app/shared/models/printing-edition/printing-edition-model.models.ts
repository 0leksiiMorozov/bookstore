import { PrintingEditionModelItem } from 'src/app/shared/models';

export class PrintingEditionModel {
  public items: Array<PrintingEditionModelItem>;
  public totalCount: number;
}
