import { FilterAscending } from 'src/app/shared/enums';

export class BaseFilterModel {
  public pageCount: number;
  public pageSize: number;
  public ascendingSort?: FilterAscending;
  public searchString?: string;
}
