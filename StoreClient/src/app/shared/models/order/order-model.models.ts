import { OrderModelItem } from 'src/app/shared/models';

export class OrderModel {

  public items: Array<OrderModelItem>;
  public totalCount: number;
}
