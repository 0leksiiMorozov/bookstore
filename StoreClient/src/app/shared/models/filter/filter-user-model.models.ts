import { FilterUserStatus, FilterColumnSort } from 'src/app/shared/enums';
import { BaseFilterModel } from 'src/app/shared/models/base/base-filter-model.models';

export class FilterUserModel extends BaseFilterModel {
  public blockState?: FilterUserStatus;
  public columnSort?: FilterColumnSort;
}
