import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import { TokensModel } from '../models/account/tokens-model.models';
import { AccountService } from '../services/account.service';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private accountService: AccountService,
    private localStorageService: LocalStorageService
    ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>>  {
    const tokens = this.localStorageService.getTokens();
    if (tokens !== null) {
      request = this.addTokensToHeader(request, tokens);
    }
    return next.handle(request).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && error.status === 401) {
        return this.handle401Error(request, next, tokens);
      }
      return throwError(error);
    }));
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler, tokens: TokensModel): Observable<any> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);
      return this.accountService.refreshTokens(tokens).pipe(
        switchMap((token: any) => {
          this.isRefreshing = false;
          const newTokens = this.localStorageService.getTokens();
          this.refreshTokenSubject.next(newTokens);
          return next.handle(this.addTokensToHeader(request, newTokens));
         }));
     }
    return this.refreshTokenSubject.pipe(
      take(1),
      switchMap(() => {
        return next.handle(this.addTokensToHeader(request, tokens));
      }));
  }

  private addTokensToHeader(request: HttpRequest<any>, tokens: TokensModel) {
    return request.clone({
      headers: request.headers.set('RefreshToken', tokens.refreshToken),
      setHeaders: {
        Authorization: `Bearer ${tokens.accessToken}`
      }
    });
  }
}
