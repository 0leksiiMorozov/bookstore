import { Component, OnInit } from '@angular/core';
import { MatIconRegistry, MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { AccountService } from 'src/app/shared/services/account.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { ConfirmPaymentComponent } from 'src/app/cart/confirm-payment/confirm-payment.component';
import { UserModelItem, CartModelItem } from 'src/app/shared/models';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser: UserModelItem;
  currentCart: CartModelItem;
  imagesPath: string;
  image: any;

  constructor(
    private dialog: MatDialog,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    public router: Router,
    private accountService: AccountService,
    private localStorageService: LocalStorageService
  ) {
    this.currentUser = new UserModelItem();
    this.currentCart = new CartModelItem();
    this.imagesPath = environment.imagesPath;
    this.image = null;

    iconRegistry.addSvgIcon('book', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/book.svg`))
    .addSvgIcon('cart', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/cart.svg`))
    .addSvgIcon('avatar', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/avatar.svg`));
  }

  ngOnInit(): void {
    this.localStorageService.user.subscribe(data => {
      this.currentUser = data;
      // let objectURL = 'data:image/png;base64,' + data.avatar;
      // this.image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      // debugger
    });
    this.localStorageService.cart.subscribe(data => {
      this.currentCart = data;
    });
  }

  isRoleAdmin(): boolean {
    if (this.isLoggedIn()) {
      return this.currentUser.role === 'Admin' ? true : false;
    }
  }

  isLoggedIn(): boolean {
    return this.localStorageService.isLoggedIn();
  }

  logOut(): void {
    this.localStorageService.removeUser();
    this.localStorageService.removeCart();
    this.accountService.logOut().subscribe();
    this.router.navigate(['account/logIn']);
  }

  openCartDialog(): void {
    let items = this.localStorageService.getCartItems();
    if (items === null) {
      return;
    }
    const dialogRef = this.dialog.open(ConfirmPaymentComponent, {
      width: '800px',
      data: {}
    });
    dialogRef.afterClosed().subscribe();
  }
}
