import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { FilterUserModel } from 'src/app/shared/models/filter/filter-user-model.models';
import { UserModel } from 'src/app/shared/models/user/user-model.models';
import { UserEditModel } from 'src/app/shared/models/user/user-edit-model.models';

import { environment } from 'src/environments/environment';
import { UserModelItem } from '../models';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl: string;

constructor(
  private http: HttpClient
) {
  this.apiUrl = environment.apiUrl;
  }

  getAllUsers(model: FilterUserModel): Observable<UserModel> {
    return this.http.post<UserModel>(`${this.apiUrl}/user/getAllUsers`,
    model,
    {withCredentials: true});
  }

  deleteUser(id: number) {
    return this.http.get(`${this.apiUrl}/user/deleteUser?id=${id}`, {withCredentials: true});
  }

  blockUser(id: number, blockState: boolean) {
    return this.http.get(`${this.apiUrl}/user/blockUser?id=${id}&block=${blockState}`, {withCredentials: true});
  }

  editUserProfile(model: UserModelItem): Observable<UserModelItem> {
    return this.http.post<UserModelItem>(`${this.apiUrl}/user/editProfile`,
    model,
    {withCredentials: true});
  }

  setPhoto(model: UserModelItem): Observable<UserModelItem> {
    return this.http.post<UserModelItem>(`${this.apiUrl}/user/setPhoto`,
    model,
    {withCredentials: true});
  }
}
