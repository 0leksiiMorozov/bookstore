import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CartModelItem, OrderModelItem } from 'src/app/shared/models';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  apiUrl: string;

constructor(
  private http: HttpClient,
) {
  this.apiUrl = environment.apiUrl;
}

  createOrder(model: CartModelItem): Observable<OrderModelItem> {
    debugger
    return this.http.post<OrderModelItem>(`${this.apiUrl}/order/createOrder`, model, {withCredentials: true});
  }
}
