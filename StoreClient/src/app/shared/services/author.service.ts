import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { AuthorModel } from 'src/app/shared/models/author/author-model.models';
import { FilterAuthorModel } from 'src/app/shared/models/filter/filter-author-model.models';
import { AuthorModelItem } from 'src/app/shared/models/author/author-model-item.models';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  apiUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.apiUrl = environment.apiUrl;
   }

  getAllAuthors(model: FilterAuthorModel): Observable<AuthorModel> {
    return this.http.post<AuthorModel>(`${this.apiUrl}/author/getAllAuthors`, model, {withCredentials: true});
  }

  createAuthor(model: AuthorModelItem): Observable<AuthorModelItem> {
    return this.http.post<AuthorModelItem>(`${this.apiUrl}/author/createAuthor`, model, {withCredentials: true});
  }

  updateAuthor(model: AuthorModelItem): Observable<AuthorModelItem> {
    return this.http.post<AuthorModelItem>(`${this.apiUrl}/author/updateAuthor`, model, {withCredentials: true});
  }

  deleteAuthor(id: number): Observable<AuthorModelItem>  {
    return this.http.get<AuthorModelItem>(`${this.apiUrl}/author/deleteAuthor?id=${id}`, {withCredentials: true});
  }

  getAuthors(): Observable<AuthorModel> {
    return this.http.get<AuthorModel>(`${this.apiUrl}/author/getAuthorsForProductCreation`,
    {withCredentials: true}
    );
  }
}
