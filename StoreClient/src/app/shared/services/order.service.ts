import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { OrderModel } from 'src/app/shared/models/order/order-model.models';
import { FilterOrderModel } from 'src/app/shared/models/filter/filter-order-model.models';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  apiUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.apiUrl = environment.apiUrl;
   }

  getAllOrders(model: FilterOrderModel): Observable<OrderModel> {
    return this.http.post<OrderModel>(`${this.apiUrl}/order/getAllOrders`, model, {withCredentials: true});
  }

  getUserOrders(id: number): Observable<OrderModel> {
    return this.http.get<OrderModel>(`${this.apiUrl}/order/getUserOrders?id=${id}`, {withCredentials: true});
  }
}
