import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { FilterPrintingEditionModel } from 'src/app/shared/models/filter/filter-printing-edition-model.models';
import { PrintingEditionModel } from 'src/app/shared/models/printing-edition/printing-edition-model.models';
import { PrintingEditionModelItem } from 'src/app/shared/models/printing-edition/printing-edition-model-item.models';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrintingEditionService {
  apiUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.apiUrl = environment.apiUrl;
   }

  getAllPrintingEditions(model: FilterPrintingEditionModel): Observable<PrintingEditionModel> {
    return this.http.post<PrintingEditionModel>(`${this.apiUrl}/printingEdition/getAllPrintingEditions`,
      model,
      {withCredentials: true}
      );
  }

  createPrintingEdition(model: PrintingEditionModelItem): Observable<PrintingEditionModelItem> {
    return this.http.post<PrintingEditionModelItem>(`${this.apiUrl}/printingEdition/createPrintingEdition`,
      model,
      {withCredentials: true}
      );
  }

  getPrintingEditionById(id: number): Observable<PrintingEditionModelItem> {
    return this.http.get<PrintingEditionModelItem>(`${this.apiUrl}/printingEdition/getPrintingEditionById?id=${id}`,
      {withCredentials: true}
      );
  }

  updatePrintingEdition(model: PrintingEditionModelItem): Observable<PrintingEditionModelItem> {
    return this.http.post<PrintingEditionModelItem>(`${this.apiUrl}/printingEdition/updatePrintingEdition`,
    model,
    {withCredentials: true}
    );
  }

  deletePrintingEdition(id: number) {
    return this.http.get(`${this.apiUrl}/printingEdition/deletePrintingEdition?id=${id}`, {withCredentials: true});
  }
}
