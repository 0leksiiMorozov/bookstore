import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { LogInModel } from 'src/app/shared/models/account/log-in-model.models';
import { RegistrationModel } from 'src/app/shared/models/account/registration-model.models';
import { Observable, throwError } from 'rxjs';
import { UserModelItem } from 'src/app/shared/models/user/user-model-item.models';
import { TokensModel } from 'src/app/shared/models/account/tokens-model.models';
import { tap, catchError } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { BaseService } from './base/base.service';
import { BaseModel } from '../models';

@Injectable({
  providedIn: 'root'
})
export class AccountService extends BaseService {
  private readonly accessToken: string;
  private readonly refreshToken: string;
  apiUrl: string;

  constructor(
    private http: HttpClient,
    private localstorageService: LocalStorageService
   ) {
      super();
      this.apiUrl = environment.apiUrl;
  }

  logIn(model: LogInModel): Observable<UserModelItem> {
    model.rememberMe = true;
    return this.http.post<UserModelItem>(`${this.apiUrl}/account/logIn`, model, { withCredentials: true }).pipe(

      catchError(this.handleErrors)
      // tap((data: UserModelItem) => {
      //   debugger
      //   if (data.errors) {
      //     this.handleErrors(data.errors);
      //   }
      // })
    );
  }

  logOut(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/account/logOut`);
  }

  registrate(registrationModel: RegistrationModel): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/account/registration`, registrationModel, {withCredentials: true});
  }

  resetPassword(email: string): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/account/resetPassword?email=${email}`, null, {withCredentials: true});
  }

  refreshTokens(tokens: TokensModel): Observable<TokensModel> {
    return this.http.post<TokensModel>(`${this.apiUrl}/account/refreshTokens`,
      {RefreshToken: tokens.refreshToken},
      {withCredentials: true});
     }
}
