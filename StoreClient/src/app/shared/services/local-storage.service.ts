import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject } from 'rxjs';

import { UserModelItem, TokensModel, CartModelItem, OrderItemModelItem } from 'src/app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  orderItemModel: Array<OrderItemModelItem>;
  private readonly accessToken: string;
  private readonly refreshToken: string;
  private currentUser = new BehaviorSubject<UserModelItem>(this.getUser());
  user = this.currentUser.asObservable();
  private currentCart = new BehaviorSubject<CartModelItem>(this.getCartItems());
  cart = this.currentCart.asObservable();

  constructor(
  private cookieService: CookieService,
  ) {
    this.accessToken = 'AccessToken';
    this.refreshToken = 'refreshtoken';
    this.orderItemModel = new Array<OrderItemModelItem>();
  }

  getTokens(): TokensModel {
    const access = this.cookieService.get('AccessToken');
    const refresh = this.cookieService.get('refreshtoken');
    const model = new TokensModel(access, refresh);
    return model;
  }

  storeUser(user: UserModelItem): void {
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUser.next(user);
  }

  getUser(): UserModelItem {
    let user = JSON.parse(localStorage.getItem('user'));
    return user;
  }

  removeUser(): void {
    localStorage.removeItem('user');
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('user') ? true : false;
  }

  getRole(): string {
    let tokens = this.getTokens();
    let dekodedToken = jwt_decode(tokens.accessToken);
    let role: string = dekodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
    return role;
  }

  checkRole(role: string): boolean {
    return this.currentUser.value.role === role ? true : false;
  }

  private storeFirstItem(cartModelItem: OrderItemModelItem ): void {
    let cartModel = new CartModelItem();
    this.orderItemModel.push(cartModelItem);
    cartModel.orderItems = this.orderItemModel;
    localStorage.setItem('cartItem', JSON.stringify(cartModel));
    this.currentCart.next(cartModel);
  }

  storeCartItem(cartModelItem: OrderItemModelItem ): void {
    let cartModel = this.getCartItems();
    if (cartModel === null) {
      this.storeFirstItem(cartModelItem);
    }
    if (cartModel !== null) {
      this.orderItemModel = this.getCartItems().orderItems;
      this.orderItemModel.push(cartModelItem);
      cartModel.orderItems = this.orderItemModel;
      localStorage.setItem('cartItem', JSON.stringify(cartModel));
      this.currentCart.next(cartModel);
    }
  }

  getCartItems(): CartModelItem {
    return JSON.parse(localStorage.getItem('cartItem'));
  }

  removeCart(): void {
    localStorage.removeItem('cartItem');
  }

  removeItemFromCart(product: OrderItemModelItem): boolean {
    debugger
    let cartModel = this.getCartItems();
    for (let i = 0; i < cartModel.orderItems.length; i++ ) {
      if (cartModel.orderItems[i].printingEditionId === product.printingEditionId) {
        cartModel.orderItems.splice(i, 1);
      }
    }
    localStorage.setItem('cartItem', JSON.stringify(cartModel));
    let emptyCartResult = this.isCartEmpty(cartModel);
    this.currentCart.next(cartModel);
    return emptyCartResult;
  }

  isCartEmpty(cartModel: CartModelItem): boolean {
    if (cartModel.orderItems.length === 0) {
      this.removeCart();
      return true;
    }
    return false;
  }
}
