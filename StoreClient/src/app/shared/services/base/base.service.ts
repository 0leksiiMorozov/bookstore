import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor() { }

  handleErrors(error: HttpErrorResponse) {
    if (error instanceof HttpErrorResponse) {
      return Observable.throw(error.message || 'Server Error');
    }
  }
}
