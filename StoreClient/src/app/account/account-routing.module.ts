import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrationComponent, ConfirmEmailComponent, ResetPasswordComponent, LogInComponent } from 'src/app';

export const routes: Routes = [
    {path: 'confirmEmail', component: ConfirmEmailComponent },
    {path: 'logIn', component: LogInComponent},
    {path: 'registration', component: RegistrationComponent},
    {path: 'resetPassword', component: ResetPasswordComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
