import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { LogInModel, UserModelItem } from 'src/app/shared/models';
import { AccountService} from 'src/app/shared/services/account.service';
import { BaseModel } from 'src/app/shared/models/base/base-model.models';

import { ValidationConstants, FilterConstants } from 'src/app/shared/constants';
import { environment } from 'src/environments/environment';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css'],
  providers: [AccountService]
})

export class LogInComponent implements OnInit {
  logInForm: FormGroup;
  logInModel: LogInModel;
  imagesPath: string;
  passwordPattern: string;
  currentUser: UserModelItem;
  error: string[];

constructor(
  private iconRegistry: MatIconRegistry,
  private sanitizer: DomSanitizer,
  private accountService: AccountService,
  private localStorageService: LocalStorageService,
  private router: Router
  ) {
    this.passwordPattern = ValidationConstants.passwordPattern;
    this.imagesPath = environment.imagesPath;
    iconRegistry.addSvgIcon('userIcon', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/userIcon.svg`));

    this.logInModel = new LogInModel();
    this.currentUser = new UserModelItem();
    this.logInForm = new FormGroup({
      email: new FormControl(FilterConstants.emptyLine, [Validators.required, Validators.email, Validators.minLength(4)] ),
      password: new FormControl(FilterConstants.emptyLine, [Validators.required, Validators.pattern(this.passwordPattern)]),
      rememberMe: new FormControl(FilterConstants.emptyLine)
    });
    this.error = [];
  }

  ngOnInit(): void {
    this.localStorageService.user.subscribe(data => {
      this.currentUser = data;
    });
  }

  checkRole(): boolean {
    return this.localStorageService.checkRole('Admin');
  }

  isLoggedIn(): boolean {
    return this.localStorageService.isLoggedIn();
  }

  logIn(): void {
    this.accountService.logIn(this.logInForm.value).subscribe(
      (data: UserModelItem) => {
        this.localStorageService.storeUser(data);
        if (this.currentUser.role === 'Admin') {
          this.router.navigate(['/printingEdition/getAllPrintingEditions']);
        }
        if (this.currentUser.role === 'User') {
          this.router.navigate(['/printingEdition/home']);
        }
      });
  }
}

