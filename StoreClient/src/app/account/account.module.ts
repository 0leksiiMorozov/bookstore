import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';

import { RegistrationComponent, ConfirmEmailComponent, ResetPasswordComponent, LogInComponent } from 'src/app';

import {routes} from 'src/app/account/account-routing.module';

@NgModule({
  declarations: [
  RegistrationComponent,
  ConfirmEmailComponent,
  ResetPasswordComponent,
  LogInComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ]
})
export class AccountModule { }
