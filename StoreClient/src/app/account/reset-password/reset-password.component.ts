import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl  } from '@angular/forms';

import { AccountService } from 'src/app/shared/services/account.service';
import { FilterConstants } from 'src/app/shared/constants';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
  providers: [AccountService]
})
export class ResetPasswordComponent {
  public resetForm: FormGroup;

  constructor(
    private accountService: AccountService
    ) {
      this.resetForm = new FormGroup({
        email: new FormControl(FilterConstants.emptyLine, [Validators.required, Validators.email])
      });
     }

     get email() {
       return this.resetForm.get('email');
     }

  resetPassword() {
    this.accountService.resetPassword(this.resetForm.value).subscribe();
  }
}
