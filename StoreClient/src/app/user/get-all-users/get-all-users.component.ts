import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import {MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';

import { EntityConstants, FilterConstants } from 'src/app/shared/constants';
import {FilterUserModel, UserModel } from 'src/app/shared/models';
import {FilterUserStatus, FilterAscending, FilterColumnSort} from 'src/app/shared/enums';
import { UserService } from 'src/app/shared/services/user.service';
import { EditUserProfileComponent } from 'src/app/user/dialogs/edit-user-profile/edit-user-profile.component';
import { DeleteComponent } from 'src/app/shared/components/delete/delete.component';
import { environment } from 'src/environments/environment';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';


@Component({
  selector: 'app-get-all-users',
  templateUrl: './get-all-users.component.html',
  styleUrls: ['./get-all-users.component.css'],
  providers: [UserService]
})
export class GetAllUsersComponent implements OnInit {
  users: UserModel;
  filterForm: FormGroup;
  filterModel: FilterUserModel;
  tableColumns: string[];
  status: string[];
  userStatus: FormControl;
  totalUsersCount: number;
  pageSizeOptions: number[];
  imagePath: string;

  constructor(
    private localStorageService: LocalStorageService,
    private userService: UserService,
    private dialog: MatDialog,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private router: Router,
    private formBuilder: FormBuilder
    ) {
      this.imagePath = environment.imagesPath;

      iconRegistry.addSvgIcon(
        'pencil', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagePath}/pencil.svg`)).addSvgIcon(
        'delete', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagePath}/delete.svg`)).addSvgIcon(
        'search', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagePath}/search.svg`));
      this.users = new UserModel();
      this.filterModel = new FilterUserModel();
      this.tableColumns = EntityConstants.userTableColumns;
      this.status = EntityConstants.userState;
      this.pageSizeOptions = EntityConstants.pageSizeOptions;

      this.filterForm = this.formBuilder.group({
      searchString: [FilterConstants.emptyLine, Validators.minLength(3)],
      columnSort: [FilterColumnSort.name],
      ascendingSort: [FilterAscending.asc],
      pageSize: [5],
      pageCount: [1]
      });
      this.userStatus = new FormControl(this.status);
     }

  ngOnInit() {
    if (!this.localStorageService.checkRole('Admin')) {
      this.router.navigate(['/printingEdition/home']);
    }
    this.getUsers();
  }

  setPaginationOptions($event): void {
    this.filterForm.controls.pageCount.setValue($event.pageIndex + 1);
    this.filterForm.controls.pageSize.setValue($event.pageSize);
    this.getUsers();
  }

  getUsers(): void {
    this.setUserStatus(this.userStatus.value);
    this.filterModel = this.filterForm.value;
    this.userService.getAllUsers(this.filterModel).subscribe(data => {
      this.totalUsersCount = data.totalCount;
      this.users = data;
      });
  }
  setUserStatus(userStatus: string[]): void {
    const status: Array<FilterUserStatus> = new Array<FilterUserStatus>();
    if (userStatus.length === 2) {
      this.filterModel.blockState = FilterUserStatus.All;
      return;
    }
    userStatus.forEach(element => {
    status.push(FilterUserStatus[element]);
    });
    this.filterModel.blockState = status[0];
  }

  setAscendingSort(direction: string, column: string) {
    const sortDirection: FilterAscending = FilterAscending[direction];
    this.filterForm.controls.ascendingSort.setValue(sortDirection);

    const sortColumn: FilterColumnSort = FilterColumnSort[column];
    this.filterForm.controls.columnSort.setValue(sortColumn);

    this.getUsers();
  }

  searchStringMinCharactersCheck(): void {
    if (this.filterForm.controls.searchString.value.length >= 3 ||
      this.filterForm.controls.searchString.value.length === 0) {
      this.getUsers();
    }
  }

  openEditDialog(id: number, firstName: string, lastName: string): void {
    const dialogRef = this.dialog.open(EditUserProfileComponent, {
      width: '500px',
      data: {id, firstName, lastName}
    });
    dialogRef.afterClosed().subscribe(() => {
        this.getUsers();
    });
  }

  openDeleteDialog(id: number): void {
    let nameEntityToDelete = 'User';
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '400px',
      data: {nameEntityToDelete}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data === true ) {
        this.userService.deleteUser(id);
      }
      this.getUsers();
    });
  }

  blockUser(id: number, blockState: boolean): void {
    this.userService.blockUser(id, blockState).subscribe();
  }
}

