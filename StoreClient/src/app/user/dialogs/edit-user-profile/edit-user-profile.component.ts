import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { UserModelItem } from 'src/app/shared/models';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-edit-user-profile',
  templateUrl: './edit-user-profile.component.html',
  styleUrls: ['./edit-user-profile.component.css']
})
export class EditUserProfileComponent {
  messageFromChild: string;
  public userModel = new UserModelItem();

  constructor(
    private userService: UserService,
    public dialogRef: MatDialogRef<EditUserProfileComponent>,
    @Inject(MAT_DIALOG_DATA) public user: UserModelItem) {}

  cancelButton(): void {
    this.dialogRef.close();
  }

  saveChanges() {
    this.userService.editUserProfile(this.user).subscribe();
  }
}
