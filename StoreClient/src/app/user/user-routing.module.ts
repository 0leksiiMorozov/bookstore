import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GetAllUsersComponent, EditUserProfileComponent, ProfileComponent } from 'src/app';

export const routes: Routes = [
  {path: 'getAllUsers', component: GetAllUsersComponent},
  {path: 'editUserProfile', component: EditUserProfileComponent},
  {path: 'profile', component: ProfileComponent}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
