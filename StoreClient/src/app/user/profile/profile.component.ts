import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { ValidationConstants, FilterConstants } from 'src/app/shared/constants';
import { UserModelItem, UserModel } from 'src/app/shared/models';
import { UserService, LocalStorageService } from 'src/app/shared/services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: UserModelItem;
  updateForm: FormGroup;
  imagesPath: string;
  passwordPattern: string;
  @Input() userModel: UserModel;
  appearPhotoInput: boolean;
  selectedFile: File;
  confirmPassword: FormControl;
  image: any;

  constructor(
    private localStorageService: LocalStorageService,
    private userService: UserService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.user = new UserModelItem();
    this.passwordPattern = ValidationConstants.passwordPattern;
    this.imagesPath = environment.imagesPath;
    this.appearPhotoInput = false;
    this.selectedFile = null;

    iconRegistry.addSvgIcon('userIcon', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/userIcon.svg`));

    this.updateForm = formBuilder.group({
      firstName: [this.user.firstName, [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
      lastName: [this.user.lastName, [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
      email: [this.user.email, [Validators.required, Validators.email]],
      oldPassword: [FilterConstants.emptyLine, [Validators.required, Validators.pattern(this.passwordPattern)]],
      newPassword: [FilterConstants.emptyLine, [Validators.required, Validators.pattern(this.passwordPattern)]],
      avatar: [FilterConstants.emptyLine]
    });
    this.confirmPassword = new FormControl(FilterConstants.emptyLine, [Validators.required, Validators.pattern(this.passwordPattern)]);
   }

   ngOnInit(): void {
    this.getUserData();
  }

  getUserData(): void {
    this.user = this.localStorageService.getUser();
    this.image = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/png;base64, ${this.user.avatar}`);
  }

  matchPassword(): boolean {
    return this.updateForm.get('newPassword').value === this.confirmPassword.value ? true : false;
  }

  editProfile(): void {
    this.user.oldPassword = this.updateForm.controls.oldPassword.value;
    this.user.newPassword = this.updateForm.controls.newPassword.value;
    this.userService.editUserProfile(this.user).subscribe( () => {
      this.localStorageService.storeUser(this.user);
      this.getUserData();
      this.refreshPage();
    });
  }

  refreshPage(): void {
    window.location.reload();
  }

  displayOrHideInput(): void {
    this.appearPhotoInput = !this.appearPhotoInput;
  }

  onFileSelected(event): void {
    this.selectedFile = event.target.files;
    let reader = new FileReader();
    reader.onload = this.handleReaderLoaded.bind(this);
    reader.readAsBinaryString(this.selectedFile[0]);
  }

  private handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.user.avatar = btoa(binaryString);
}

  setPhoto(): void {
    this.userService.setPhoto(this.user).subscribe(data => {
      this.localStorageService.storeUser(data);
      this.getUserData();
      this.refreshPage();
    });
  }
}
