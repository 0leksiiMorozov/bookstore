import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialog, MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

import { AuthorService } from 'src/app/shared/services/author.service';
import { AuthorModel, AuthorModelItem } from 'src/app/shared/models';
import { FilterAscending, FilterAuthorByColumn } from 'src/app/shared/enums';
import { EntityConstants, FilterConstants } from 'src/app/shared/constants';
import { environment } from 'src/environments/environment';
import { AddAuthorComponent } from 'src/app/author/dialog/add-author/add-author.component';
import { DeleteComponent } from 'src/app/shared/components/delete/delete.component';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-get-all-authors',
  templateUrl: './get-all-authors.component.html',
  styleUrls: ['./get-all-authors.component.css'],
  providers: [AuthorService]
})
export class GetAllAuthorsComponent implements OnInit {

  authors: AuthorModel;
  filterForm: FormGroup;
  totalAuthorsCount: number;
  tableColumns: string[];
  productTableColumns: string[];
  pageSizeOptions: number[];
  pageSize: number;
  imagesPath: string;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private authorService: AuthorService,
    private dialog: MatDialog,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder
    ) {
      this.imagesPath = environment.imagesPath;

      iconRegistry.addSvgIcon(
        'pencil', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/pencil.svg`)).addSvgIcon(
        'delete', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/delete.svg`)).addSvgIcon(
        'search', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/search.svg`)).addSvgIcon(
        'add', sanitizer.bypassSecurityTrustResourceUrl(`${this.imagesPath}/add.svg`));

      this.authors = new AuthorModel();
      this.tableColumns = EntityConstants.authorTableColumns;
      this.pageSizeOptions = EntityConstants.pageSizeOptions;

      this.filterForm = this.formBuilder.group({
      searchString: [FilterConstants.emptyLine, Validators.minLength(3)],
      columnSort: [FilterAuthorByColumn.id],
      ascendingSort: [FilterAscending.asc],
      pageSize: [FilterConstants.pageSize],
      pageCount: [FilterConstants.pageCount]
      });
     }

  ngOnInit(): void {
    if (!this.localStorageService.checkRole('Admin')) {
      this.router.navigate(['/printingEdition/home']);
    }
    this.getAuthors();
  }

  setPaginationOptions($event): void {
    this.filterForm.controls.pageCount.setValue($event.pageIndex + 1);
    this.filterForm.controls.pageSize.setValue($event.pageSize);
    this.getAuthors();
  }

  searchStringMinCharactersCheck(): void {
    if (this.filterForm.controls.searchString.value.length >= 3 ||
      this.filterForm.controls.searchString.value.length === 0) {
      this.getAuthors();
    }
  }

  getAuthors(): void {
      this.authorService.getAllAuthors(this.filterForm.value).subscribe(
        (data: AuthorModel) => {
          this.totalAuthorsCount = data.totalCount;
          this.authors = data;
    });
  }

  setAscendingSort(direction: string, column: string): void {
    const sortDirection: FilterAscending = FilterAscending[direction];
    this.filterForm.controls.ascendingSort.setValue(sortDirection);

    const sortColumn: FilterAuthorByColumn = FilterAuthorByColumn[column];
    this.filterForm.controls.columnSort.setValue(sortColumn);

    this.getAuthors();
  }

  openCreateAuthorDialog(): void {
    let author = new AuthorModelItem();
    let actionString = 'Create';
    const dialogRef = this.dialog.open(AddAuthorComponent, {
      width: '400px',
      data: {author, actionString}
    });
    dialogRef.afterClosed().subscribe(() => {
        this.getAuthors();
    });
  }

  openUpdateAuthorDialog(author: AuthorModelItem): void {
    let actionString = 'Update';
    const dialogRef = this.dialog.open(AddAuthorComponent, {
      width: '400px',
      data: {author, actionString}
    });
    dialogRef.afterClosed().subscribe(() => {
        this.getAuthors();
    });
  }

  openDeleteAuthorDialog(id: number): void {
    let nameEntityToDelete = 'Author';
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '300px',
      data: {nameEntityToDelete}
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data === true) {
        this.authorService.deleteAuthor(id).subscribe();
      }
      this.getAuthors();
    });
  }
}
