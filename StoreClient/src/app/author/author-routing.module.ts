import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GetAllAuthorsComponent, AddAuthorComponent } from 'src/app';


export const routes: Routes = [
  {path: 'getAllAuthors', component: GetAllAuthorsComponent},
  {path: 'addAuthor', component: AddAuthorComponent}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorRoutingModule { }
