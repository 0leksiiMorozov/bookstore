import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

import { GetAllAuthorsComponent, AddAuthorComponent } from 'src/app';

import { routes } from 'src/app/author/author-routing.module';

@NgModule({
  declarations: [
    GetAllAuthorsComponent,
    AddAuthorComponent
    ],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AuthorModule { }
