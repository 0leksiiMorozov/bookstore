import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { AuthorService } from 'src/app/shared/services/author.service';
import { AuthorDialogModel } from 'src/app/shared/models/author/author-dialog-model.models';

@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.css']
})
export class AddAuthorComponent {

  constructor(
    private authorService: AuthorService,
    private dialogRef: MatDialogRef<AddAuthorComponent>,
    @Inject(MAT_DIALOG_DATA) public authorModel: AuthorDialogModel
    ) { }

  canselButton(): void {
    this.dialogRef.close();
  }

  chooseMethod(actionString: string) {
    if (actionString === 'Create') {
      this.createAuthor();
    }
    this.updateAuthor();
  }

  createAuthor() {
    this.authorService.createAuthor(this.authorModel.author).subscribe();
  }

  updateAuthor() {
    this.authorService.updateAuthor(this.authorModel.author).subscribe();
  }
}
