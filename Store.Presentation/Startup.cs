using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Store.BusinessLogicLayer.Initialization;
using Store.DataAccessLayer.Initialization;
using Store.Presentation.Helpers;
using Store.Presentation.Helpers.Interfaces;
using Store.Presentation.Middlewares;
using System;
using static Store.BusinessLogicLayer.Common.Constants.Jwt.Constants.JwtConstants;

namespace Store.Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            var initializer = new Initializer();
            initializer.Initialize(services, Configuration.GetConnectionString("DefaultConnection"));
            var signingKey = new JwtHelper();
            services.AddTransient<IJwtHelper, JwtHelper>();
            
            var signingDecodingKey = (IJwtHelper)signingKey;
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(jwtBearerOptions =>
                {
                    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = signingDecodingKey.GetKey(),
                        ValidateIssuer = true,
                        ValidIssuer = ValidIssuer,
                        ValidateAudience = true,
                        ValidAudience = ValidAudience,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };                   
                })
                .AddCookie();
            services.AddCors();
            services.AddMvcCore().AddApiExplorer();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DataBaseInitialization dataBaseInitialization)
        {
            dataBaseInitialization.InitializeAll();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCors(options => options.WithOrigins("http://localhost:4200").AllowCredentials().AllowAnyHeader().AllowAnyMethod());
            app.UseRouting();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });           
            app.UseMiddleware<LoggerMiddleware>();
        }
    }
}
