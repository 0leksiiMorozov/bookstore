﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogicLayer.Models.Authors;
using Store.BusinessLogicLayer.Services.Interfaces;
using System.Threading.Tasks;

namespace Store.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpPost("createAuthor")]
        public async Task<IActionResult> CreateAuthor(AuthorsModelItem model)
        {
            var result = await _authorService.CreateAuthorAsync(model);
            return Ok(result);
        }

        [HttpPost("updateAuthor")]
        public async Task<IActionResult> UpdateAuthor(AuthorsModelItem model)
        {
            var result = await _authorService.UpdateAuthorAsync(model);
            return Ok(result);
        }

        [HttpGet("deleteAuthor")]
        public async Task<IActionResult> DeleteAuthor(long id)
        {
            var result = await _authorService.DeleteAuthorAsync(id);
            return Ok(result);
        }

        [HttpPost("getAllAuthors")]
        public async Task<IActionResult> GetAllAuthors(AuthorsBllFilteringModel model)
        {
            var result = await _authorService.GetAllAuthorsAsync(model);
            return Ok(result);
        }

        [HttpGet("getAuthorsForProductCreation")]
        public async Task<IActionResult> GetAuthorsForProductCreation()
        {
            var result = await _authorService.GetAuthorsForProductCreationAsync();
            return Ok(result);
        }
    }
}