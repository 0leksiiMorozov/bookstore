﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogicLayer.Models.Account;
using Store.BusinessLogicLayer.Services.Interfaces;
using Store.Presentation.Helpers.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using static Store.BusinessLogicLayer.Common.Constants.Email.Constants;
using static Store.BusinessLogicLayer.Common.Constants.Error.Constants;
using static Store.BusinessLogicLayer.Common.Constants.Jwt.Constants;

namespace Store.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IJwtHelper _jwtHelper;

        public AccountController(IAccountService accountService, IJwtHelper jwtHelper)
        {
            _accountService = accountService;
            _jwtHelper = jwtHelper;
        }

        [HttpPost("registration")]
        public async Task<IActionResult> Registration(RegistrationModel model)
        {
            var result = await _accountService.RegisterAsync(model);
            if (result.Errors.Any())
            {
                return Ok(result);
            }
            var userModel = await _accountService.GetUserByEmailAsync(model.Email);
            var code = await _accountService.GenerateEmailConfirmationTokenAsync(userModel);
            if (string.IsNullOrWhiteSpace(code))
            {
                result.Errors.Add(ErrorConstants.ImpossibleToFindUser);
                return Ok(result);
            }
            var callbackUrl = Url.Action(
                nameof(AccountController.ConfirmEmail),
                "Account",
                new { userId = userModel.Id, code = code },
                protocol: HttpContext.Request.Scheme
                );
            result = await _accountService.SendMailAsync(model.Email, callbackUrl, EmailConstants.ConfirmationSubject);
            return Ok(result);
        }

        [HttpPost("logIn")]
        public async Task<IActionResult> LogIn(LogInModel model)
        {         
            var result = await _accountService.LogInAsync(model, model.Password);
            if (result.Errors.Any())
            {
                return Ok(result);
            }
            var tokens = _jwtHelper.GenerateTokens(result);
            Response.Cookies.Append(JwtConstants.AccessToken, tokens.AccessToken );
            Response.Cookies.Append(JwtConstants.RefreshToken, tokens.RefreshToken );
            return Ok(result);
        }

        [Authorize]
        [HttpGet("logOut")]
        public async Task<IActionResult> LogOut()
        {
            await _accountService.LogOutAsync();
            return Ok();
        }

        [HttpGet("confirmEmail")]
        public async Task<IActionResult> ConfirmEmail(long userId, string code)
        {
            var result = await _accountService.ConfirmEmailAsync(userId, code);
            return Ok(result);
        }

        [HttpPost("resetPassword")]
        public async Task<IActionResult> ResetPassword(string email)
        {
            var result = await _accountService.IsEmailConfirmedAsync(email);
            if (result.Errors.Any())
            {
                return Ok(result);
            }
            result = await _accountService.ResetPasswordAsync(email);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("refreshTokens")]
        public async Task<IActionResult> RefreshTokens()
        {
            var token = HttpContext.Request.Headers
                .Where(x => x.Key == JwtConstants.RefreshToken)
                .Select(x => x.Value).FirstOrDefault();

            var userId = _jwtHelper.GetIdFromToken(token);
            var user = await _accountService.GetUserByIdAsync(userId);
            var result = await _accountService.IsLockedOutAsync(user);
            if (result.Errors.Any())
            {
                return Ok(result);
            }
            var newTokenPair = _jwtHelper.RefreshOldTokens(user, token);
            HttpContext.Response.Cookies.Append(JwtConstants.AccessToken, newTokenPair.AccessToken);
            HttpContext.Response.Cookies.Append(JwtConstants.RefreshToken, newTokenPair.RefreshToken);
            return Ok();
        }
    }
}