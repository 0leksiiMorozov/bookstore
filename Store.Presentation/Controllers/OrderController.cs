﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogicLayer.Models.Cart;
using Store.BusinessLogicLayer.Models.Orders;
using Store.BusinessLogicLayer.Services.Interfaces;
using Store.Presentation.Helpers.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using static Store.BusinessLogicLayer.Common.Constants.Jwt.Constants;

namespace Store.Presentation.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IJwtHelper _jwtHelper;
        public OrderController(IOrderService orderService, IJwtHelper jwtHelper)
        {
            _orderService = orderService;
            _jwtHelper = jwtHelper;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("getAllOrders")]
        public async Task<IActionResult> GetAllOrders(OrdersBllFilterModel model)
        {
            var orders = await _orderService.GetAllOrdersAsync(model);
            return Ok(orders);
        }

        [HttpPost("createOrder")]
        public async Task<IActionResult> CreateOrder(CartModelItem model)
        {
            var token = HttpContext.Request.Headers
                .Where(x => x.Key == JwtConstants.RefreshToken)
                .Select(x => x.Value).FirstOrDefault();
            var userId = _jwtHelper.GetIdFromToken(token);
            var result = await _orderService.MakeOrderAsync(model, userId);                                                           
            return Ok(result);
        }  
        
        [HttpGet("getUserOrders")]
        public async Task<IActionResult> GetUserOrders(long id)
        {
            var orders = await _orderService.GetUserOrdersAsync(id);
            return Ok(orders);
        }
    }
}