﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogicLayer.Models.PrintingEdition;
using Store.BusinessLogicLayer.Services.Interfaces;
using System.Threading.Tasks;

namespace Store.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class PrintingEditionController : ControllerBase
    {
        private readonly IPrintingEditionService _printingEditionService;

        public PrintingEditionController(IPrintingEditionService printingEditionService)
        {
            _printingEditionService = printingEditionService;
        }

        [AllowAnonymous]
        [HttpPost("getAllPrintingEditions")]
        public async Task<IActionResult> GetAllPrintingEditions(PrintingEditionsBllFilterModel model)
        {
            var result = await _printingEditionService.GetAllPrintingEditionsAsync(model);
            return Ok(result);
        }

        [HttpPost("createPrintingEdition")]
        public async Task<IActionResult> CreatePrintingEdition(PrintingEditionsModelItem model)
        {
            var result = await _printingEditionService.CreatePrintingEditionAsync(model);
            return Ok(result);
        }

        [HttpPost("updatePrintingEdition")]
        public async Task<IActionResult> UpdatePrintingEdition(PrintingEditionsModelItem model)
        {
            var result = await _printingEditionService.UpdatePrintingEditionAsync(model);
            return Ok(result);
        }

        [HttpGet("deletePrintingEdition")]
        public async Task<IActionResult> DeletePrintingEdition(long id)
        {
            var result = await _printingEditionService.DeletePrintingEditionAsync(id);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("getPrintingEditionById")]
        public async Task<IActionResult> GetPrintingEditionById(long id)
        {
            var result = await _printingEditionService.GetPrintingEditionById(id);
            return Ok(result);
        }
    }
}