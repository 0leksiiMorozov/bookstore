﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogicLayer.Models.Users;
using Store.BusinessLogicLayer.Services.Interfaces;
using Store.Presentation.Helpers.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Store.BusinessLogicLayer.Common.Constants.Jwt.Constants;

namespace Store.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IJwtHelper _jwtHelper;

        public UserController(IUserService userService, IJwtHelper jwtHelper)
        {
            _userService = userService;
            _jwtHelper = jwtHelper;
        }

        [HttpPost("getAllUsers")]
        public async Task<IActionResult> GetAllUsers(UsersBllFilterModel model)
        {
            var token = HttpContext.Request.Headers
                .Where(x => x.Key == JwtConstants.RefreshToken)
                .Select(x => x.Value).FirstOrDefault();
            var adminId = _jwtHelper.GetIdFromToken(token);
            var users = await _userService.GetAllUsersAsync(model, adminId);
            return Ok(users);
        }

        [HttpGet("blockUser")]
        public async Task<IActionResult> BlockUser(long id, bool block)
        {
            var result = await _userService.BlockUserAsync(id, block);
            return Ok(result);
        }

        [HttpGet("deleteUser")]
        public async Task<IActionResult> DeleteUser(long id)
        {
            var result = await _userService.DeleteUserAsync(id);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("editProfile")]
        public async Task<IActionResult> EditProfile(UserModelItem model)
        {
            var token = HttpContext.Request.Headers
                .Where(x => x.Key == JwtConstants.RefreshToken)
                .Select(x => x.Value).FirstOrDefault();
            var adminId = _jwtHelper.GetIdFromToken(token);

            var result = await _userService.EditUserAsync(model, adminId);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("setPhoto")]
        public async Task<IActionResult> SetPhoto(UserModelItem model)
        {
            var token = HttpContext.Request.Headers
                .Where(x => x.Key == JwtConstants.RefreshToken)
                .Select(x => x.Value).FirstOrDefault();
            var userId = _jwtHelper.GetIdFromToken(token);
            var result = await _userService.SetPhotoAsync(model);
            return Ok((UserModelItem)result);
        }
    }
}